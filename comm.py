import glob, os, sys
import pandas as pd
import numpy as np
import uproot

file_AllCoeff = pd.read_csv('/eos/user/m/mtobin/UT/ANALYSIS/ChipIDs_ALL.csv',delimiter=",",names = ['stave', 'Chip0', 'Chip1', 'Chip2', 'Chip3'])
dirData = '/eos/lhcb/user/c/chmancus/DATA/'

fname = glob.glob("/eos/user/m/mtobin/UT/DATA/*/*/*/UT*/2022/*/*/Chip*/data_200V_-*0C*+MIP_2022*.raw.gz")
root = []
image = []

for f in fname:
    root.append(f[29:])
    image.append(f[29:-7] + '_mcm.png')

existingImage = []
indImage = 0
indImage = []
ccc = 0
for i in image:
    if (os.path.exists(dirData + i)):
        existingImage.append(i)
        indImage.append(ccc)
    ccc = ccc + 1

resultRoot = [i.split('/') for i in root]
resultImage = [i.split('/') for i in existingImage]
dfAll = pd.DataFrame(resultRoot,  columns=['A_C', 'IP_MAG', 'X_U', 'stave', 'year', 'month', 'day', 'nChip', 'fileName'])
dfImage = pd.DataFrame(resultImage, columns=['A_C', 'IP_MAG', 'X_U', 'stave', 'year', 'month', 'day', 'nChip', 'fileName'])

df3 = dfLook.append(dfImage_unique)


for ac, ipmag, xu, st, y, m, d, nC, fN in df3.itertuples(index=False):
    if ac == 'A':
        f = dirData + 'A/' + ipmag + '/' + xu + '/' + st + '/' + str(y) + '/' + str(m) + '/' + str(d) + '/' + nC + '/' + fN
    else:
        if ipmag == 'IP':
            f = dirData + 'C/' + ipmag + '/' + xu + '/' + st + '/' + str(y) + '/' + str(m) + '/' + str(d) + '/' + nC + '/' + fN
        else:
            f = dirData + 'C/' + ipmag + '/' + xu + '/' + fN
    comm = 'gzip -d {}'.format(f)
    os.system(comm)
    fd = f[:-3]
    result = np.array(np.where(file_AllCoeff['stave'] == st))
    fChip = file_AllCoeff
    if (result.size > 1):
        print('Found more than one stave in the Chip file')
    comm1 = './Salt.exe -f {} -a {} -m mcm'.format(fd, fChip.iloc[result[0][0]][nC])
    os.system(comm1)
    comm2 = 'gzip -9 {}'.format(fd)
    os.system(comm2)
    

