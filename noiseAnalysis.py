import glob, os
import pandas as pd
import numpy as np
import ROOT
import mplhep
import matplotlib.pyplot as plt
import uproot
import re
from scipy.optimize import curve_fit
from termcolor import colored
from ast import literal_eval
import matplotlib as mpl
from matplotlib.ticker import NullFormatter, MaxNLocator
plt.style.use(mplhep.style.ATLAS)
dirData = '/eos/lhcb/user/c/chmancus/DATA/'

def gaussian(x, A, mu, sigma):
    return A * np.exp(-(x - mu)*(x - mu)/(2.0 * sigma * sigma ))

## Before executing the code, be sure to set the right location for the plots to be saved. The name for those created for no inj, +MIP, -MIP is the same, so change location or modify the name
def compareRms(df):
    df_rms = pd.DataFrame(columns = ['stave', 'nChip', 'mean_Raw', 'mean_Cms', 'rms_Raw', 'rms_Cms', 'nBadC', 'lBadC'])
    for index, i in df.iterrows():
        appoR = []
        appoR = i['rms_AdcR'].copy()
        appoC = []
        appoC = i['rms_AdcC'].copy()
        badC = []
        nBadC = []
        try:
            counts22_R, bin_edges22_R = np.histogram(appoR, bins=30)
            binc_R = 0.5*(bin_edges22_R[:-1]+bin_edges22_R[1:])
            par_R, var_R = curve_fit(gaussian, binc_R, counts22_R, p0=[15,np.mean(appoR),0.07])
            counts22_C, bin_edges22_C = np.histogram(appoC, bins=30)
            binc_C = 0.5*(bin_edges22_C[:-1]+bin_edges22_C[1:])
            par_C, var_C = curve_fit(gaussian, binc_C, counts22_C, p0=[15,np.mean(appoC),0.07])
            for j in np.arange(len(appoR)):
                if ((appoR[j]> par_R[1]+5*abs(par_R[2])) or (appoR[j]< par_R[1]-5*abs(par_R[2]))):
                    print('Bad channel found Raw: {} {} {}'.format(i['stave'], i['nChip'], j))
                if ((appoC[j]> par_C[1]+5*abs(par_C[2])) or (appoC[j]< par_C[1]-5*abs(par_C[2]))):
                    print('Bad channel found Cms: {} {} {}'.format(i['stave'], i['nChip'], j))
                    badC.append(appoC[j])
                    nBadC.append(j)
        except:
            print(colored('Gaussian fit failed!', 'red'))
            counts22, bin_edges22 = np.histogram(appoC, bins=30)
            mplhep.histplot(counts22, bins=bin_edges22, histtype="errorbar", ax=None, color="black")
            plt.xlabel('Noise [LSB]')
            plt.ylabel('Events')
            plt.savefig('./plots/gaussFailed_{}_{}.pdf'.format(i['stave'], i['nChip']))
            plt.close()
            par_R = []
            par_R.append(0)
            par_R.append(np.mean(appoR))
            par_R.append(np.std(appoR))
            par_C = []
            par_C.append(0)
            par_C.append(np.mean(appoC))
            par_C.append(np.std(appoC))
            for j in np.arange(len(appoR)):
                if ((appoR[j]> np.mean(appoR)+5*np.std(appoR)) or (appoR[j]< np.mean(appoR)-5*np.std(appoR))):
                    print('Bad channel found Raw: {} {} {}'.format(i['stave'], i['nChip'], j))
                if ((appoC[j]> np.mean(appoC)+5*np.std(appoC)) or (appoC[j]< np.mean(appoC)-5*np.std(appoC))):
                    print('Bad channel found Cms: {} {} {}'.format(i['stave'], i['nChip'], j))
                    badC.append(appoC[j])
                    nBadC.append(j)
        bC = len(nBadC)
        if bC > 30:
            counts22_C, bin_edges22_C = np.histogram(appoC, bins=30)
            mplhep.histplot(counts22_C, bins=bin_edges22_C, histtype="errorbar", ax=None, color="black")
            plt.xlabel('Noise [LSB]')
            plt.ylabel('Events')
            plt.savefig('./plots/manyBC_{}_{}.pdf'.format(i['stave'], i['nChip']))
            plt.close()
        plt.plot([], [], ' ', label="{} {}".format(i['stave'], i['nChip']))
        plt.plot([], [], ' ', label="{}".format(i['fileName'][0][5:]))
        for nn, bl in enumerate(nBadC):
            plt.axvline(x = bl, ymin=0, ymax=badC[nn], color = 'red', alpha = 0.5)
        plt.scatter(np.arange(128), appoR, marker = '.', color = 'green', label = 'Adc raw')
        plt.scatter(np.arange(128), appoC, marker = '.', color = 'blue', label = 'Adc Cms')
        plt.plot(nBadC, badC, marker = '.', color = 'red', label = 'Bad channel')
        plt.ylim(0,2)
        plt.xlim(0,128)
        plt.plot(np.arange(128), np.ones(128)*par_R[1], color = 'dimgray', label = 'Mean adc raw {}'.format(round(par_R[1],3)), linestyle='dotted')
        plt.plot(np.arange(128), np.ones(128)*par_C[1], color = 'darkgray', label = 'Mean adc cms {}'.format(round(par_C[1],3)), linestyle='dotted')
        df_rms.loc[index] = [i['stave'], i['nChip'], par_R[1], par_C[1], par_R[2], par_C[2], bC, nBadC]
        plt.legend(loc='best')
        plt.xlabel('Channel')
        plt.ylabel('Noise [LSB]')
        # plt.show()
        plt.savefig('./plots_C/noise_{}_{}.pdf'.format(i['stave'], i['nChip']))
        plt.close()
        print('{} {}: mean = {} nBadChannels = {}'.format(i['stave'], i['nChip'], np.nanmean(appoC), bC))
        print('-------------------------------')

def comparePed(df_C, df_MIP_C):
    xlabel = 'Channels'
    ylabel = 'AdcRaw'
    left, width = 0.12, 0.55
    bottom, height = 0.12, 0.55
    bottom_h = left_h = left+width+0.02
    rect_temperature = [left, bottom, width, height] # dimensions of temp plot
    rect_histx = [left, bottom_h, width, 0.25] # dimensions of x-histogram
    rect_histy = [left_h, bottom, 0.2, height] # dimensions of y-histogram
    ymin = -20
    ymax = 28
    xmin = 0
    xmax = 128
    nxbins = 128
    nybins = 64
    nbins = 100
    xbins = np.linspace(start = xmin, stop = xmax, num = nxbins)
    ybins = np.linspace(start = ymin, stop = ymax, num = nybins)
    xcenter = (xbins[0:-1]+xbins[1:])/2.0
    ycenter = (ybins[0:-1]+ybins[1:])/2.0
    nBad = 0
    badC = 0
    df_ampl = pd.DataFrame(columns = ['stave', 'nChip', 'mean_Raw', 'mean_RawMIP', 'amp', 'nBadC', 'lBadC'])
    for index, i in df_MIP_C.iterrows():
        aa = 0
        if(np.any(np.array(list(df_C.loc[(df_C['stave'] == i.stave)&(df_C['nChip'] == i.nChip)]['mean_AdcR'])))):
            aa = np.abs(np.array(i.mean_AdcR) - np.array(list(df_C.loc[(df_C['stave'] == i.stave)&(df_C['nChip'] == i.nChip)]['mean_AdcR'][np.where((df_C['stave'] == i.stave)&(df_C['nChip'] == i.nChip))[0][0]])[0]))
            if len(aa[aa<5]) > 0:
                nBad = len(aa[aa<5])
                badC = np.where(aa[aa<5])[0]
            # df_ampl.loc[index] = [i.stave, i.nChip, list(df_C.loc[(df_C['stave'] == i.stave)&(df_C['nChip'] == i.nChip)]['mean_AdcR'][np.where((df_C['stave'] == i.stave)&(df_C['nChip'] == i.nChip))[0][0]])[0], i.mean_AdcR, aa, nBad, badC]
            fig = plt.figure(1, figsize=(9.5,9))
            axTemperature = plt.axes(rect_temperature) # temperature plot
            axHistx = plt.axes(rect_histx) # x histogram
            axHisty = plt.axes(rect_histy) # y histogram
            nullfmt = NullFormatter()
            axHistx.xaxis.set_major_formatter(nullfmt)
            axHisty.yaxis.set_major_formatter(nullfmt)
            matrix = np.concatenate((df_C.loc[(df_C['stave'] == i.stave)&(df_C['nChip'] == i.nChip)]['adcRaw'][np.where((df_C['stave'] == i.stave)&(df_C['nChip'] == i.nChip))[0][0]], i.adcRaw), axis =1 )
            H, xedges,yedges = np.histogram2d(np.repeat(np.arange(matrix.shape[0]), matrix.shape[1]), np.ravel(matrix), range = [[0,127], [-20,28]], bins=(128, 45))
            X = xcenter
            Y = ycenter
            Z = H
            cax = (axTemperature.hist2d(np.repeat(np.arange(matrix.shape[0]), matrix.shape[1]), np.ravel(matrix), range = [[0,127], [-20,28]], bins=(128, 45), cmap = 'Blues', norm=mpl.colors.LogNorm()))
            axTemperature.plot([], [], ' ', label="{} {}".format(i.stave, i.nChip))
            axTemperature.plot([], [], ' ', label="{}".format(i.fileName[:-5]))
            cc = 0
            for ncc, aaa in enumerate(aa):
                if aaa<5:
                    axTemperature.axvline(x = ncc, ymin=0, ymax=28, color = 'red', alpha = 0.5)
                    cc = cc +1
            df_ampl.loc[index] = [i.stave, i.nChip, list(df_C.loc[(df_C['stave'] == i.stave)&(df_C['nChip'] == i.nChip)]['mean_AdcR'][np.where((df_C['stave'] == i.stave)&(df_C['nChip'] == i.nChip))[0][0]])[0], i.mean_AdcR, aa, cc, badC]
            axTemperature.plot([], [], ' ', label="#Bad Channels = {}".format(cc))
            axTemperature.legend(prop={'size':10.5,'family':'serif'}, loc = 'lower right')
            contourcolor = 'white'
            axTemperature.set_xlabel(xlabel,fontsize=12)
            axTemperature.set_ylabel(ylabel,fontsize=12)
            ticklabels = axTemperature.get_xticklabels()
            for label in ticklabels:
                label.set_fontsize(12)
                label.set_family('serif')
            ticklabels = axTemperature.get_yticklabels()
            for label in ticklabels:
                label.set_fontsize(12)
                label.set_family('serif')
            xbins = np.arange(xmin, xmax, (xmax-xmin)/128)
            ybins = np.arange(ymin, ymax, (ymax-ymin)/64)
            xb = np.linspace(0,128,129)
            yb = np.linspace(-20, 28, 45)
            counts22_R, bin_edges22_R, p = axHisty.hist(np.ravel(df_C.loc[(df_C['stave'] == i.stave)&(df_C['nChip'] == i.nChip)]['adcRaw'][np.where((df_C['stave'] == i.stave)&(df_C['nChip'] == i.nChip))[0][0]]), bins=yb, orientation='horizontal', color = 'green', alpha = 0.5)
            binc_R = 0.5*(bin_edges22_R[:-1]+bin_edges22_R[1:])
            try:
                par_R, var_R = curve_fit(gaussian, binc_R, counts22_R, p0=[150,np.mean(np.sum(df_C.loc[(df_C['stave'] == i.stave)&(df_C['nChip'] == i.nChip)]['adcRaw'][np.where((df_C['stave'] == i.stave)&(df_C['nChip'] == i.nChip))[0][0]], axis = 1)/np.shape(df_C.loc[(df_C['stave'] == i.stave)&(df_C['nChip'] == i.nChip)]['adcRaw'][np.where((df_C['stave'] == i.stave)&(df_C['nChip'] == i.nChip))[0][0]])[1]),5])
                axHisty.plot(gaussian(np.linspace(par_R[1]-par_R[2]*5, par_R[1]+par_R[2]*5, 1000), *par_R), np.linspace(par_R[1]-par_R[2]*5, par_R[1]+par_R[2]*5, 1000), color = 'green', label= r'Gaussian fit no inj.'+\
                ' \n $\mu =$ %s'%round(par_R[1],1) +\
                ' \n $\sigma =$ %s'%round(par_R[2],1)
                )
                axHistx.plot(np.linspace(0,128,1000), np.linspace(par_R[1], par_R[1], 1000), color = 'seagreen', linewidth = 0.8)
                axHistx.plot(np.linspace(0,128,1000), np.linspace(par_R[1]+par_R[2]*3, par_R[1]+par_R[2]*3, 1000), color = 'seagreen', linestyle ='--', linewidth = 0.8)
                axHistx.plot(np.linspace(0,128,1000), np.linspace(par_R[1]-par_R[2]*3, par_R[1]-par_R[2]*3, 1000), color = 'seagreen', linestyle ='--', linewidth = 0.8)
            except:
                pass
            axHistx.stairs(np.sum(df_C.loc[(df_C['stave'] == i.stave)&(df_C['nChip'] == i.nChip)]['adcRaw'][np.where((df_C['stave'] == i.stave)&(df_C['nChip'] == i.nChip))[0][0]], axis = 1)/np.shape(df_C.loc[(df_C['stave'] == i.stave)&(df_C['nChip'] == i.nChip)]['adcRaw'][np.where((df_C['stave'] == i.stave)&(df_C['nChip'] == i.nChip))[0][0]])[1], xb, color = 'green')
            counts22_R, bin_edges22_R, p = axHisty.hist(np.ravel(i.adcRaw), bins=yb, orientation='horizontal', alpha = 0.5, color = 'red')
            binc_R = 0.5*(bin_edges22_R[:-1]+bin_edges22_R[1:])
            try:
                par_R, var_R = curve_fit(gaussian, binc_R, counts22_R, p0=[150,np.mean(np.sum(i.adcRaw, axis = 1)/np.shape(i.adcRaw)[1]),5])
                axHisty.plot(gaussian(np.linspace(par_R[1]-par_R[2]*5, par_R[1]+par_R[2]*5, 1000), *par_R), np.linspace(par_R[1]-par_R[2]*5, par_R[1]+par_R[2]*5, 1000), label=r'Gaussian fit $+$ MIP'+\
                ' \n $\mu =$ %s'%round(par_R[1],1) +\
                ' \n $\sigma =$ %s'%round(par_R[2],1) , color = 'red')
                axHistx.plot(np.linspace(0,128,1000), np.linspace(par_R[1], par_R[1], 1000), color = 'maroon', linewidth = 0.8)
                axHistx.plot(np.linspace(0,128,1000), np.linspace(par_R[1]+par_R[2]*3, par_R[1]+par_R[2]*3, 1000), color = 'maroon', linestyle ='--', linewidth = 0.8)
                axHistx.plot(np.linspace(0,128,1000), np.linspace(par_R[1]-par_R[2]*3, par_R[1]-par_R[2]*3, 1000), color = 'maroon', linestyle ='--', linewidth = 0.8)
            except:
                pass
            axHisty.legend(prop={'size':10.5,'family':'serif'})
            axHistx.stairs(np.sum(i.adcRaw, axis = 1)/np.shape(i.adcRaw)[1], xb, color = 'red')
            axHistx.set_xlim(xmin, xmax)
            axHisty.set_ylim(ymin, ymax)
            ticklabels = axHistx.get_yticklabels()
            for label in ticklabels:
                label.set_fontsize(12)
                label.set_family('serif')
            ticklabels = axHisty.get_xticklabels()
            for label in ticklabels:
                label.set_fontsize(12)
                label.set_family('serif')
            axHisty.xaxis.set_major_locator(MaxNLocator(4))
            axHistx.yaxis.set_major_locator(MaxNLocator(4))
            plt.savefig('/eos/user/c/chmancus/plots_C/amp_{}_{}.pdf'.format(i.stave, i.nChip))
            plt.close()
        else:
            print("Missing file {} {}".format(i.stave, i.nChip))

