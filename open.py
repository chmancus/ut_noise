import glob, os
import pandas as pd
import numpy as np
import ROOT
import mplhep
import matplotlib.pyplot as plt
import uproot
import re
from scipy.optimize import curve_fit
from termcolor import colored
from ast import literal_eval
import matplotlib as mpl
from matplotlib.ticker import NullFormatter, MaxNLocator
plt.style.use(mplhep.style.ATLAS)
dirData = '/eos/lhcb/user/c/chmancus/DATA/'

df = pd.read_csv('sideA.csv', sep = '\t')
df_MIP = pd.read_csv('sideA_+MIP.csv', sep = '\t')
df_C = pd.read_csv('sideC.csv', sep = '\t')
df_MIP_C = pd.read_csv('sideC_+MIP.csv', sep = '\t')

mcm = []
adcR = []
adcC = []
comm = []

for index, i in df_MIP.iterrows():
    f = dirData + 'A/' + i.IP_MAG + '/' + i.X_U + '/' + i.stave + '/' + str(i.year) + '/' + str(i.month) + '/' + str(i.day) + '/' + i.nChip + '/' + i.fileName +'.root'
    try:
        fi = uproot.open(f)
        t = fi['tree']
        mcmDist = t['MCMDist'].array()
        adcRaw = t['AdcRaw'].array(library='pd')
        adcRaw = np.array(adcRaw)
        adcRaw = adcRaw.reshape(len(mcmDist),128)
        adcRaw = adcRaw.T
        adcCms = t['AdcCms'].array(library='pd')
        adcCms = np.array(adcCms)
        adcCms = adcCms.reshape(len(mcmDist),128)
        adcCms = adcCms.T
        mcm.append(mcmDist)
        adcR.append(adcRaw)
        adcC.append(adcCms)
    except:
        print('File not found')
        pass

df_MIP['adcRaw'] = adcR
df_MIP['adcCms'] = adcC
df_MIP['mcmDist'] = mcm

df_MIP['rms_AdcR'] = df_MIP['rms_AdcR'].apply(literal_eval)
df_MIP['rms_AdcC'] = df_MIP['rms_AdcC'].apply(literal_eval)
df_MIP['rms20_AdcR'] = df_MIP['rms20_AdcR'].apply(literal_eval)
df_MIP['rms20_AdcC'] = df_MIP['rms20_AdcC'].apply(literal_eval)
df_MIP['mean_AdcR'] = df_MIP['mean_AdcR'].apply(literal_eval)
df_MIP['mean_AdcC'] = df_MIP['mean_AdcC'].apply(literal_eval)
df_MIP['mean20_AdcR'] = df_MIP['mean20_AdcR'].apply(literal_eval)
df_MIP['mean20_AdcC'] = df_MIP['mean20_AdcC'].apply(literal_eval)

mcm = []
adcR = []
adcC = []
comm = []

for index, i in df.iterrows():
    f = dirData + 'A/' + i.IP_MAG + '/' + i.X_U + '/' + i.stave + '/' + str(i.year) + '/' + str(i.month) + '/' + str(i.day) + '/' + i.nChip + '/' + i.fileName +'.root'
    try:
        fi = uproot.open(f)
        t = fi['tree']
        mcmDist = t['MCMDist'].array()
        adcRaw = t['AdcRaw'].array(library='pd')
        adcRaw = np.array(adcRaw)
        adcRaw = adcRaw.reshape(len(mcmDist),128)
        adcRaw = adcRaw.T
        adcCms = t['AdcCms'].array(library='pd')
        adcCms = np.array(adcCms)
        adcCms = adcCms.reshape(len(mcmDist),128)
        adcCms = adcCms.T
        mcm.append(mcmDist)
        adcR.append(adcRaw)
        adcC.append(adcCms)
    except:
        print('File not found')
        pass

df['adcRaw'] = adcR
df['adcCms'] = adcC
df['mcmDist'] = mcm

df['rms_AdcR'] = df['rms_AdcR'].apply(literal_eval)
df['rms_AdcC'] = df['rms_AdcC'].apply(literal_eval)
df['rms20_AdcR'] = df['rms20_AdcR'].apply(literal_eval)
df['rms20_AdcC'] = df['rms20_AdcC'].apply(literal_eval)
df['mean_AdcR'] = df['mean_AdcR'].apply(literal_eval)
df['mean_AdcC'] = df['mean_AdcC'].apply(literal_eval)
df['mean20_AdcR'] = df['mean20_AdcR'].apply(literal_eval)
df['mean20_AdcC'] = df['mean20_AdcC'].apply(literal_eval)

mcm = []
adcR = []
adcC = []
comm = []

for index, i in df_C.iterrows():
    if ipmag == 'IP':
        f = dirData + 'C/' + i.IP_MAG + '/' + i.X_U + '/' + i.stave + '/' + str(i.year) + '/' + str(i.month) + '/' + str(i.day) + '/' + i.nChip + '/' + i.fileName +'.root'
    else:
        f = dirData + 'C/' + i.IP_MAG + '/' + i.X_U + '/' + i.fileName +'.root'
    try:
        fi = uproot.open(f)
        t = fi['tree']
        mcmDist = t['MCMDist'].array()
        adcRaw = t['AdcRaw'].array(library='pd')
        adcRaw = np.array(adcRaw)
        adcRaw = adcRaw.reshape(len(mcmDist),128)
        adcRaw = adcRaw.T
        adcCms = t['AdcCms'].array(library='pd')
        adcCms = np.array(adcCms)
        adcCms = adcCms.reshape(len(mcmDist),128)
        adcCms = adcCms.T
        mcm.append(mcmDist)
        adcR.append(adcRaw)
        adcC.append(adcCms)
    except:
        print('File not found')
        pass

df_C['adcRaw'] = adcR
df_C['adcCms'] = adcC
df_C['mcmDist'] = mcm

df_C['rms_AdcR'] = df_C['rms_AdcR'].apply(literal_eval)
df_C['rms_AdcC'] = df_C['rms_AdcC'].apply(literal_eval)
df_C['rms20_AdcR'] = df_C['rms20_AdcR'].apply(literal_eval)
df_C['rms20_AdcC'] = df_C['rms20_AdcC'].apply(literal_eval)
df_C['mean_AdcR'] = df_C['mean_AdcR'].apply(literal_eval)
df_C['mean_AdcC'] = df_C['mean_AdcC'].apply(literal_eval)
df_C['mean20_AdcR'] = df_C['mean20_AdcR'].apply(literal_eval)
df_C['mean20_AdcC'] = df_C['mean20_AdcC'].apply(literal_eval)

mcm = []
adcR = []
adcC = []
comm = []

for index, i in df_MIP_C.iterrows():
    if ipmag == 'IP':
        f = dirData + 'C/' + i.IP_MAG + '/' + i.X_U + '/' + i.stave + '/' + str(i.year) + '/' + str(i.month) + '/' + str(i.day) + '/' + i.nChip + '/' + i.fileName +'.root'
    else:
        f = dirData + 'C/' + i.IP_MAG + '/' + i.X_U + '/' + i.fileName +'.root'
    try:
        fi = uproot.open(f)
        t = fi['tree']
        mcmDist = t['MCMDist'].array()
        adcRaw = t['AdcRaw'].array(library='pd')
        adcRaw = np.array(adcRaw)
        adcRaw = adcRaw.reshape(len(mcmDist),128)
        adcRaw = adcRaw.T
        adcCms = t['AdcCms'].array(library='pd')
        adcCms = np.array(adcCms)
        adcCms = adcCms.reshape(len(mcmDist),128)
        adcCms = adcCms.T
        mcm.append(mcmDist)
        adcR.append(adcRaw)
        adcC.append(adcCms)
    except:
         print('gzip -d ' + f[:-5]+'.raw.gz')
         print('./Salt.exe -f ' + f[:-5]+'.raw -m mcm -a \n')
        pass

df_MIP_C['adcRaw'] = adcR
df_MIP_C['adcCms'] = adcC
df_MIP_C['mcmDist'] = mcm

df_MIP_C['rms_AdcR'] = df_MIP_C['rms_AdcR'].apply(literal_eval)
df_MIP_C['rms_AdcC'] = df_MIP_C['rms_AdcC'].apply(literal_eval)
df_MIP_C['rms20_AdcR'] = df_MIP_C['rms20_AdcR'].apply(literal_eval)
df_MIP_C['rms20_AdcC'] = df_MIP_C['rms20_AdcC'].apply(literal_eval)
df_MIP_C['mean_AdcR'] = df_MIP_C['mean_AdcR'].apply(literal_eval)
df_MIP_C['mean_AdcC'] = df_MIP_C['mean_AdcC'].apply(literal_eval)
df_MIP_C['mean20_AdcR'] = df_MIP_C['mean20_AdcR'].apply(literal_eval)
df_MIP_C['mean20_AdcC'] = df_MIP_C['mean20_AdcC'].apply(literal_eval)
